// version info
#define CHAI3D_DESCRIPTION     "Haptic Simulation Framework"
#define CHAI3D_VERSION_MAJOR   3
#define CHAI3D_VERSION_MINOR   3
#define CHAI3D_VERSION_PATCH   0
#define CHAI3D_PRERELEASE      "-pre"
#define CHAI3D_METADATA        "+gc11dda393"
#define CHAI3D_VERSION         "3.3.0-pre+gc11dda393"
#define CHAI3D_VERSION_FULL    "3.3.0-pre+gc11dda393"
#define CHAI3D_REVISION        0xc11dda393

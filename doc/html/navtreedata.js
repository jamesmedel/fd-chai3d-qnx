/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "chai3d", "index.html", [
    [ "Introduction", "index.html", null ],
    [ "Overview", "overview.html", "overview" ],
    [ "Documentation", "modules.html", "modules" ]
  ] ]
];

var NAVTREEINDEX =
[
"chapter1-introduction.html",
"classchai3d_1_1c_algorithm_potential_field.html#a9ec6c2db2cc2fb3cc781c93beacfbe22",
"classchai3d_1_1c_delta_device.html#a813eb4d624302df8eef48d378f76d3a7",
"classchai3d_1_1c_generic_haptic_device.html#a70725780b2b63559a3dbc1745286f53c",
"classchai3d_1_1c_generic_tool.html#a0e8a20589c05ba24f9398ba1a5b046e5",
"classchai3d_1_1c_image.html#a7f05c49bab5dff231903fbcfda337add",
"classchai3d_1_1c_multi_point.html#a3061665ac19739ad4589a0b259abeb68",
"classchai3d_1_1c_shader.html#a7acce523b2f433d37c2619301324b446",
"classchai3d_1_1c_spot_light.html",
"classchai3d_1_1c_video.html#a838005a5ded1fbc087bda3052cf7a0cd",
"structchai3d_1_1c_collision_a_a_b_b_node.html",
"structchai3d_1_1c_colorf.html#a380ed2f1be25bf1c8f88b77820778c05",
"structchai3d_1_1c_material.html#a525cf7dcb3a7551e0f454b1db06cf965",
"structchai3d_1_1c_matrix3d.html#ae89c5c71063844efc186d51faa7f5c35"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
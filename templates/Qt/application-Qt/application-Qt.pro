#  Copyright (C) 2001-2022 Force Dimension, Switzerland.
#  All Rights Reserved.
#
#  chai3d 3.3.0-pre+gc11dda393
#  Haptic Simulation Framework
#
#  THIS FILE CAN NOT BE COPIED AND/OR DISTRIBUTED WITHOUT EXPRESS
#  PERMISSION FROM FORCE DIMENSION.

# project
TEMPLATE = app
DESTDIR = ../../../bin/$$OS-$$ARCH
QT = core gui widgets svg

# Qt 6 support
greaterThan(QT_MAJOR_VERSION, 5) {
    QT += openglwidgets
}

# build settings
MOC_DIR = ./moc
UI_DIR = ./ui
RCC_DIR = ./rcc
CONFIG += c++11
win32: {
    OBJECTS_DIR += ./obj/$(Configuration)/$(Platform)
    CONFIG += windows
}
unix: {
    OBJECTS_DIR += ./obj/$$CFG/$$OS-$$ARCH-$$COMPILER
}
unix:!macx: {
    DEFINES += LINUX
}
macx: {
    DEFINES += MACOSX
    CONFIG += app_bundle sdk_no_version_check
    QMAKE_MACOSX_DEPLOYMENT_TARGET = $$OSXVER
    QMAKE_MAC_SDK = macosx$$OSXVER
}

# compiler-specific settings
win32: {
    QMAKE_CXXFLAGS_WARN_ON -= -w34100
}
!win32: {
    QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter
}

# CHAI3D dependency
INCLUDEPATH += ../../../src
INCLUDEPATH += ../../../externals/Eigen
INCLUDEPATH += ../../../externals/glew/include
win32: {
    PRE_TARGETDEPS += ../../../lib/$(Configuration)/$(Platform)/chai3d.lib
    LIBS += ../../../lib/$(Configuration)/$(Platform)/chai3d.lib opengl32.lib glu32.lib
}
unix: {
    PRE_TARGETDEPS += ../../../lib/$$CFG/$$OS-$$ARCH-$$COMPILER/libchai3d.a
    LIBS += -L../../../lib/$$CFG/$$OS-$$ARCH-$$COMPILER -lchai3d
    LIBS += -L../../../externals/DHD/lib/$$OS-$$ARCH -ldrd
}
unix:!macx: {
    LIBS += -ldl -lpng -lGLU -lusb-1.0
}
macx: {
    LIBS += -framework CoreAudio -framework AudioToolbox -framework AudioUnit
}

# resources
RESOURCES += resources.qrc
win32: RC_FILE = win32.rc
unix:!macx: { ICON = chai3d.ico }
macx: ICON = chai3d.icns

# sources
FORMS += Interface.ui
HEADERS += Application.h Interface.h
SOURCES += main.cpp Application.cpp Interface.cpp

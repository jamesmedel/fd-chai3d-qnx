
SWIFT++ Decomposer V1.0 -- Command Line --

Usage:
  decomposer [options] input_filename
Options:
  -h -help : This help information
  -j ampl  : Jitter the input at the given amplitude
  -e err   : Edge flip using the given absolute error
  -ef file : Save edge flip results to file (.tri)

  -1       : Decompose the model into only 1 piece
  -dfs     : Run plain DFS decomposition
  -bfs     : Run plain BFS decomposition
  -cbfs    : Run cresting BFS decomposition (default)
  -df file : Save decomposition to file (.dcp)

  -hier    : Compute hierarchy
  -s split : Type of splitting when building hierarchy: MED, MID, MEAN, GAP
  -hf file : Save hierarchy to file (.chr)

Default values:
  No jittering
  No edge flipping
  Run cresting BFS decomposition
  Do not compute hierarchy
  Split type MIDPOINT (if hierarchy computed)
  No results are saved




decomposer_c.exe -dfs -j 0.1 -hf visrochet.chr -df visrochet.dcp  visrochet.tri

jitter is optional!


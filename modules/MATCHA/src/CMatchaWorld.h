//==============================================================================
/*
    Software License Agreement (BSD License)
    Copyright (c) 2003-2016, CHAI3D
    (www.chai3d.org)

    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

    * Neither the name of CHAI3D nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
    COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE. 

    \author    <http://www.chai3d.org>
    \author    Francois Conti
    \version   3.3.0-pre+gc11dda393
*/
//==============================================================================
//------------------------------------------------------------------------------
#ifndef CMatchaWorldH
#define CMatchaWorldH
//------------------------------------------------------------------------------
#include "chai3d.h"
#include "CMatchaObject.h"
//------------------------------------------------------------------------------
#include "SWIFT.h"
#include <Eigen/LU>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
namespace chai3d {
//------------------------------------------------------------------------------

//==============================================================================
/*!
    \file       CMatchaWorld.h

    \brief
    Implementation of a Matcha world.
*/
//==============================================================================

const int MATCHA_MAX_NUM_OBJECTS      = 200;
const int MATCHA_MAX_COLLISION_EVENTS = 30;
const int MATCHA_MAX_CONTACTS         = 20;
const int MATCHA_NUM_BUFFERS          = 8;

struct cMatchaContact
{
    // collision distance
    double m_distance;

    // position on object 0
    cVector3d m_globalPosition0;

    // position on object 1
    cVector3d m_globalPosition1;

    // normal on object 0
    cVector3d m_globalNormal;

    // feature 0
    int m_feature0;

    // feature 1
    int m_feature1;
};

struct cMatchaCollisionEvent
{
    cMatchaCollisionEvent()
    {
        m_numContacts = 0;
        m_object0 = NULL;
        m_object1 = NULL;
    }

    // object 0
    cGenericObject* m_object0;

    // object 1
    cGenericObject* m_object1;

    // number of contacts
    int m_numContacts;

    // array of contacts
    cMatchaContact m_contacts[MATCHA_MAX_CONTACTS];
};


//==============================================================================
/*!
    \class      cMatchaWorld
    \ingroup    Matcha

    \brief
    This class implements an Matcha world.

    \details
    cMatchaWorld implements a virtual world to handle Matcha based objects 
    (cMatchaGenericBody).
*/
//==============================================================================
class cMatchaWorld : public chai3d::cWorld
{
    //--------------------------------------------------------------------------
    // CONSTRUCTOR & DESTRUCTOR:
    //--------------------------------------------------------------------------

public:

    //! Constructor of cMatchaWorld.
    cMatchaWorld();

    //! Destructor of cMatchaWorld.
    virtual ~cMatchaWorld();


    //--------------------------------------------------------------------------
    // PUBLIC METHODS:
    //--------------------------------------------------------------------------

public:

    //! This method computes the nearest distance between all selected objects in the environment.
    bool queryExactDistance(const double& a_tolerance);

    //! This method computes all interferences between all selected objects in the environment.
    bool queryIntersection();

    //! This method returns a pointer to an object given a SWIFT ID.
    cGenericObject* getObject(int a_Id);

    //! This method activates all possible object pairs in the environment.
    bool activateAllObjects();

    //! This method activates an object with all other active objects in the environment.
    bool activateObject(cMatchaObject* a_object);

    //! This method activates a pair of objects in the environment.
    bool activatePair(cMatchaObject* a_object0, cMatchaObject* a_object1);

    //! This method deactivates all object pairs in the environment.
    bool deactivateAllObjects();

    //! This method deactivates an object in the environment.
    bool deactivateObject(cMatchaObject* a_object);

    //! This method deactivates a pair of objects in the environment.
    bool deactivatePair(cMatchaObject* a_object0, cMatchaObject* a_object1);

    //! This method moves a selected object to a desired position and orientation.
    bool moveObject(cGenericObject* a_object, cVector3d& a_desiredPos, cMatrix3d& a_desiredRot, double a_tolerance);

    // This method enables or disable the display of contact points.
    void setShowContactPoints(bool a_showContactPoints) { m_showContactPoints = a_showContactPoints; }

    // This method returns __true__ if the contact points are being displayed, __false__ otherwise.
    bool getShowContactPoints() { return (m_showContactPoints); }


    //--------------------------------------------------------------------------
    // PUBLIC MEMBERS:
    //--------------------------------------------------------------------------

public:

    //! List of all Matcha bodies in world.
    std::vector<cMatchaObject*> m_bodies;

    //! SWIFT scene
    SWIFT_Scene* m_SWIFTscene;

    //! Array containing pointers to SWIFT objects
    cGenericObject* m_SWIFTid[MATCHA_MAX_NUM_OBJECTS];


    //-----------------------------------------------------------------------
    // PROTECTED METHODS:
    //-----------------------------------------------------------------------

protected:

    //! This method renders all light sources of this world.
    virtual void render(cRenderOptions& a_options);

    // computing constrained accelerations
    void constrainedAcceleration(const Eigen::Vector3d &a, const Eigen::Vector3d &alpha,
        Eigen::Vector3d &ac, Eigen::Vector3d &alphac);


    //--------------------------------------------------------------------------
    // PROTECTED MEMBERS:
    //--------------------------------------------------------------------------

public:

    //! Array of SWIFT collision events
    cMatchaCollisionEvent m_SWIFTcollisionEvents[MATCHA_NUM_BUFFERS][MATCHA_MAX_COLLISION_EVENTS];

    //! Number of collisions
    int m_SWIFTnumCollisions;

    //! Have contacts been detected?
    bool m_SWIFTcontactDetected;

    //! Swift current buffer
    int m_SWIFTcurrentBuffer;

    //! If __true__ then display contact points.
    bool m_showContactPoints;

    // compliance of 6-dof proxy
    double m_compliance;
    Eigen::Vector3d                   m_mass, m_moment;
    std::vector<Eigen::Vector3d>      m_contactPoints;
    std::vector<Eigen::Vector3d>      m_contactNormals;
    bool m_status;
};

//------------------------------------------------------------------------------
} // namespace chai3d
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#endif
//------------------------------------------------------------------------------

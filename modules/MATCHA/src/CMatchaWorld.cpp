//==============================================================================
/*
    Software License Agreement (BSD License)
    Copyright (c) 2003-2016, CHAI3D
    (www.chai3d.org)

    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

    * Neither the name of CHAI3D nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
    COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE. 

    \author    <http://www.chai3d.org>
    \author    Francois Conti
    \version   3.3.0-pre+gc11dda393
*/
//==============================================================================

//------------------------------------------------------------------------------
#include "CMatchaWorld.h"
//------------------------------------------------------------------------------
#include "CWilhelmsenProjection.h"
//------------------------------------------------------------------------------
using namespace std;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
namespace chai3d {
//------------------------------------------------------------------------------

//==============================================================================
/*!
    Constructor of cMatchaWorld.
*/
//==============================================================================
cMatchaWorld::cMatchaWorld()
{
    // create a SWIFT scene
    m_SWIFTscene = new SWIFT_Scene(true, true);
    m_SWIFTnumCollisions = 0;
    m_SWIFTcontactDetected = false;
    m_SWIFTcurrentBuffer = 0;

    for (int i=0; i<MATCHA_MAX_NUM_OBJECTS; i++)
    {
        m_SWIFTid[i] = NULL;
    }

    // assume the object has unit mass
    m_mass << 1.0, 1.0, 1.0;

    // todo: This works much much better than summation... why?!
    const double r = 3.0;
    const double I = 1.0; //1.0 * 0.4 * r * r;

    m_moment[0] = I;
    m_moment[1] = I;
    m_moment[2] = I;

    m_showContactPoints = false;
    m_compliance = 0.1;
    m_status = false;
}


//==============================================================================
/*!
    Destructor of cMatchaWorld.
*/
//==============================================================================
cMatchaWorld::~cMatchaWorld()
{
    // clear all bodies
    m_bodies.clear();
}


//===========================================================================
/*!
   Retrieve a pointer to a part object by passing an ID reference
*/
//===========================================================================
cGenericObject* cMatchaWorld::getObject(int a_Id)
{
    if ((a_Id >= 0) && (a_Id < MATCHA_MAX_NUM_OBJECTS))
    {
        return (m_SWIFTid[a_Id]);
    }
    else
    {
        return (NULL);
    }
}


//===========================================================================
/*!
   This method computes all interferences between all selected objects in
   the environment.
*/
//===========================================================================
bool cMatchaWorld::queryIntersection()
{
    int num_pairs = 0;
    int* oids;

    bool result =  m_SWIFTscene->Query_Intersection(
                                                    true,              
                                                    num_pairs,
                                                    &oids);     // feature_ids
    return (result);
}


//===========================================================================
/*!
   This method computes the nearest distance between all selected objects in
   the environment.

   \param  a_tolerance  Distance tolerance.
*/
//===========================================================================
bool cMatchaWorld::queryExactDistance(const double& a_tolerance)
{
    int num_pairs = 0;
    int* oids;
    int* num_contacts;
    SWIFT_Real* distances;
    SWIFT_Real* nearest_pts;
    SWIFT_Real* normals;
    int* feature_types;
    int* feature_ids;

    int nbc = 0;
    cVector3d v,vr;
    double time = 0;
    bool result;

    try
    {
        result =  m_SWIFTscene->Query_Contact_Determination(
                          false,              // early exit
                          (SWIFT_Real)a_tolerance,        // tolerance
                          num_pairs,
                          &oids,
                          &num_contacts,
                          &distances,
                          &nearest_pts,
                          &normals,
                          &feature_types,      // feature_types
                          &feature_ids);     // feature_ids
    }
    catch (...)
    {
        return(false);
    }

    try
    {
    // copy all collision events to local table
    int i=0;
    while ((i < num_pairs) && (i < MATCHA_MAX_COLLISION_EVENTS))
    {
        // retrieve objects
        int id0 = oids[i<<1];
        int id1 = oids[(i<<1)+1];

        m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_object0 = getObject(id0);
        m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_object1 = getObject(id1);

        // read the number of collisions for this event (pair)
        m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_numContacts = num_contacts[i];

        // for each collision event retrieve all the collisions
        if (m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_numContacts != -1)
        {
            result = true;
            int j = 0;
            while ((j < m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_numContacts) && (j < MATCHA_MAX_CONTACTS))
            {
                // distance
                m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_contacts[j].m_distance = distances[(nbc+j)];

                // position 0
                v.set(nearest_pts[(6*(nbc+j))+0],
                        nearest_pts[(6*(nbc+j))+1],
                        nearest_pts[(6*(nbc+j))+2]);

                m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_object0->getGlobalRot().mulr(v,vr);
                m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_object0->getGlobalPos().addr(vr,vr);
                m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_contacts[j].m_globalPosition0 = vr;

                // position 1
                v.set(nearest_pts[(6*(nbc+j))+3],
                        nearest_pts[(6*(nbc+j))+4],
                        nearest_pts[(6*(nbc+j))+5]);

                m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_object1->getGlobalRot().mulr(v,vr);
                m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_object1->getGlobalPos().addr(vr,vr);
                m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_contacts[j].m_globalPosition1 = vr;

                // normal
                v.set(normals[(3*(nbc+j))+0],
                        normals[(3*(nbc+j))+1],
                        normals[(3*(nbc+j))+2]);
                m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_contacts[j].m_globalNormal = v;

                // feature 0
                m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_contacts[j].m_feature0 = feature_types[(2*(nbc+j))+0];

                // feature 1
                m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_contacts[j].m_feature1 = feature_types[(2*(nbc+j))+1];

                // increment
                j++;
            }

            // increment index counter
            nbc = nbc + m_SWIFTcollisionEvents[m_SWIFTcurrentBuffer][i].m_numContacts;
        }

        // increment
        i++;
    }

    // store number of collision events
    m_SWIFTnumCollisions = num_pairs;
        
    m_SWIFTcurrentBuffer++;
    if (m_SWIFTcurrentBuffer >= MATCHA_NUM_BUFFERS)
    {
        m_SWIFTcurrentBuffer = 0;
    }
    }
    catch (...)
    {
        return(false);
    }
    return (true);
}


//===========================================================================
/*!
    This method moves a selected object to a desired position and orientation.

    \param  a_object  Object to move in the environment.
    \param  a_desiredPos  Desired goal position of object
    \param  a_desiredRot  Desired goal orientation of object.
    \param  a_tolerance  Distance tolerance
*/
//===========================================================================
bool cMatchaWorld::moveObject(cGenericObject* a_object, cVector3d& a_desiredPos, cMatrix3d& a_desiredRot, double a_tolerance)
{
    // temp variables
    bool result;
    int num_pairs = 0;
    int* oids;
    cVector3d nearestPos;
    cMatrix3d nearestRot;


    /////////////////////////////////////////////////////////////////////
    // INITIALIZATION
    /////////////////////////////////////////////////////////////////////

    // read current position of object
    cVector3d currentPos = a_object->getLocalPos();
    cMatrix3d currentRot = a_object->getLocalRot();

    // compute directions towards desired goal
    cVector3d deltaPos = cSub(a_desiredPos, currentPos);
    double distance = deltaPos.length();

    //cMatrix3d deltaRot = cMul(a_desiredRot, cMul(cMul(cTranspose(currentRot), a_desiredRot), cTranspose(currentRot)));
    //cMatrix3d deltaRot = cTranspose(currentRot * cTranspose(a_desiredRot));
    cMatrix3d deltaRot = a_desiredRot * cTranspose(currentRot);

    cVector3d deltaRotAxis;
    double deltaRotAxisAngle;
    deltaRot.toAxisAngle(deltaRotAxis, deltaRotAxisAngle);
    deltaRotAxisAngle = deltaRotAxisAngle * m_compliance;
    if (deltaRotAxisAngle > 0.001)
    {
        deltaRotAxis.normalize();
    }
    else
    {
        deltaRotAxis.set(1, 0, 0);
    }

    // if no motion detected, then exit
    if ((distance < 0.001) && (cAbs(deltaRotAxisAngle) < 0.001))
    {
        nearestPos = currentPos;
        nearestRot = currentRot;
        a_object->setLocalPos(nearestPos);
        a_object->setLocalRot(nearestRot);
        m_status = false;
        return (true);
    }

    // perform collision first detection if necessary
    result = queryExactDistance(a_tolerance);
    if (result == false)
    {
        nearestPos = currentPos;
        nearestRot = currentRot;
        a_object->setLocalPos(nearestPos);
        a_object->setLocalRot(nearestRot);
        m_status = false;
        return (true);
    }

    // create constraint list from collision points
    if (m_SWIFTnumCollisions > 0)
    {
        m_SWIFTcontactDetected = true;
    }
    else
    {
        m_SWIFTcontactDetected = false;
    }


    // clear points
    m_contactPoints.clear();
    m_contactNormals.clear();


    int counter = 0;
    int numTotalContacts = 0;
    int MAX_CONTACTS = 999;
    for (int k = 0; k < MATCHA_NUM_BUFFERS; k++)
    {
        for (int i = 0; i < m_SWIFTnumCollisions; i++)
        {
            int numContacts = m_SWIFTcollisionEvents[k][i].m_numContacts;
            for (int j = 0; j < numContacts; j++)
            {
                if ((m_SWIFTcollisionEvents[k][i].m_object0->getOwner() == a_object) &&
                    (m_SWIFTcollisionEvents[k][i].m_object1->getOwner() != a_object))
                {
                    cVector3d position = cSub(m_SWIFTcollisionEvents[k][i].m_contacts[j].m_globalPosition0, currentPos);
                    cVector3d normal = (m_SWIFTcollisionEvents[k][i].m_contacts[j].m_globalNormal);

                    Eigen::Vector3d p(position(0), position(1), position(2));
                    Eigen::Vector3d n(normal(0), normal(1), normal(2));
                    if (numTotalContacts < MAX_CONTACTS)
                    {
                        m_contactPoints.push_back(p);
                        m_contactNormals.push_back(n);
                        numTotalContacts++;
                    }
                }

                if ((m_SWIFTcollisionEvents[k][i].m_object0->getOwner() != a_object) &&
                    (m_SWIFTcollisionEvents[k][i].m_object1->getOwner() == a_object))
                {
                    cVector3d position = cSub(m_SWIFTcollisionEvents[k][i].m_contacts[j].m_globalPosition1, currentPos);
                    cVector3d normal = cNegate(m_SWIFTcollisionEvents[k][i].m_contacts[j].m_globalNormal);

                    Eigen::Vector3d p(position(0), position(1), position(2));
                    Eigen::Vector3d n(normal(0), normal(1), normal(2));
                    if (numTotalContacts < MAX_CONTACTS)
                    {
                        m_contactPoints.push_back(p);
                        m_contactNormals.push_back(n);
                        numTotalContacts++;
                    }
                }
            }
        }
    }

    cout << numTotalContacts << endl;

    Eigen::Vector3d a(deltaPos(0), deltaPos(1), deltaPos(2));
    Eigen::Vector3d alpha(deltaRotAxisAngle * deltaRotAxis(0), deltaRotAxisAngle * deltaRotAxis(1), deltaRotAxisAngle * deltaRotAxis(2));

    Eigen::Vector3d ac, alphac;
    ac = a;
    alphac = alpha;

    if (!m_contactNormals.empty())
    {
        constrainedAcceleration(a, alpha, ac, alphac);
    }

    /*
    if ((isnan(ac[0]) != 0) || (isnan(ac[1]) != 0) || (isnan(ac[2]) != 0) ||
        (isnan(alphac[0]) != 0) || (isnan(alphac[1]) != 0) || (isnan(alphac[2]) != 0))
    {
        return (false);
    }
    */
    cVector3d projDeltaPos(ac[0], ac[1], ac[2]);
    cVector3d projDeltaRot(alphac[0], alphac[1], alphac[2]);

    double DTP = 1.0;
    double DTR = 1.0;
    bool success = false;
    counter = 0;
    cVector3d targetPos;
    cMatrix3d targetRot;

    while ((!success) && (counter < 12)) //12
    {
        counter++;
        DTP = DTP / 4.0;
        DTR = DTR / 4.0;

        targetPos = cAdd(currentPos, cMul(DTP, projDeltaPos));
        targetRot = currentRot;

        if (projDeltaRot.lengthsq() > 0)
        {
            targetRot.rotateAboutGlobalAxisRad(cNormalize(projDeltaRot), DTR * projDeltaRot.length());
        }

        a_object->setLocalPos(targetPos);
        a_object->setLocalRot(targetRot);

        try
        {
            result =  m_SWIFTscene->Query_Intersection(
                              true,              
                              num_pairs,
                              &oids);     // feature_ids

        }
        catch (...)
        {
            result = true;
        }

        if  (result == false)
        {
            success = true;
        }
    }
    if (success)
    {
        nearestPos =  targetPos;
        nearestRot =  targetRot;
        a_object->setLocalPos(nearestPos);
        a_object->setLocalRot(nearestRot);
    }
    else
    {
        targetPos =  currentPos;
        targetRot =  currentRot;
        nearestPos =  targetPos;
        nearestRot =  targetRot;
        a_object->setLocalPos(nearestPos);
        a_object->setLocalRot(nearestRot);

        // check for intersection
        result =  m_SWIFTscene->Query_Intersection(
                                                  true,              
                                                  num_pairs,
                                                  &oids);     // feature_ids

        // if a collision occurs, then step back to previous position
        if  (result == false)
        {
            success = true;
        }
        else
        {
            a_object->setLocalPos(currentPos);
            a_object->setLocalRot(currentRot);
            success = false;
        }
    }

    m_status = false;
    return (success);
}


//===========================================================================
/*!
    This method activates all possible object pairs in the environment.
*/
//===========================================================================
bool cMatchaWorld::activateAllObjects()
{
    if (m_SWIFTscene != NULL)
    {
        m_SWIFTscene->Activate();

        if (m_bodies.size() > 1)
        {
            for (unsigned int i = 0; i < m_bodies.size(); i++)
            {
                for (unsigned int j = (i+1); j <m_bodies.size(); j++)
                {
                    activatePair(m_bodies[i], m_bodies[j]);
                }
            }
        }

        return true;
    }
    else
    {
        return false;
    }
}


//===========================================================================
/*!
    This method activates an object with all other active objects in the 
    environment.

    \param  a_object  Object to be activated.
*/
//===========================================================================
bool cMatchaWorld::activateObject(cMatchaObject* a_object)
{
    if ((m_SWIFTscene != NULL) && (a_object != NULL))
    {
        for (unsigned int i = 0; i < m_bodies.size(); i++)
        {
            cMatchaObject* object = m_bodies[i];
            if (object != a_object)
            {
                activatePair(a_object, object);
            }
        }

        return true;
    }
    else
    {
        return false;
    }
}


//===========================================================================
/*!
    This method activates a pair of objects in the environment.

    \param  a_object0  Object 0 to be activated.
    \param  a_object1  Object 1 to be activated.
*/
//===========================================================================
bool cMatchaWorld::activatePair(cMatchaObject* a_object0, cMatchaObject* a_object1)
{
    if ((m_SWIFTscene != NULL) && (a_object0 != NULL) && (a_object1 != NULL))
    {
        for (unsigned int i = 0; i < a_object0->m_objects.size(); i++)
        {
            for (unsigned int j = 0; j < a_object1->m_objects.size(); j++)
            {
                m_SWIFTscene->Activate(a_object0->m_objects[i].m_SWIFTobjectId, a_object1->m_objects[j].m_SWIFTobjectId);
            }
        }

        return true;
    }
    else
    {
        return false;
    } 
}


//===========================================================================
/*!
    This method deactivates all possible object pairs in the environment.
*/
//===========================================================================
bool cMatchaWorld::deactivateAllObjects()
{
    if (m_SWIFTscene != NULL)
    {
        m_SWIFTscene->Deactivate();
        return true;
    }
    else
    {
        return false;
    }
}


//===========================================================================
/*!
    This method activates an object with all other active objects in the 
    environment.

    \param  a_object  Object to be activated.
*/
//===========================================================================
bool cMatchaWorld::deactivateObject(cMatchaObject* a_object)
{
    if ((m_SWIFTscene != NULL) && (a_object != NULL))
    {
        for (unsigned int i = 0; i < a_object->m_objects.size(); i++)
        {
            m_SWIFTscene->Deactivate(a_object->m_objects[i].m_SWIFTobjectId);
        }
        return true;
    }
    else
    {
        return false;
    }
}


//===========================================================================
/*!
    This method deactivates a pair of objects in the environment.

    \param  a_object0  Object 0 to be activated.
    \param  a_object1  Object 1 to be activated.
*/
//===========================================================================
bool cMatchaWorld::deactivatePair(cMatchaObject* a_object0, cMatchaObject* a_object1)
{
    if ((m_SWIFTscene != NULL) && (a_object0 != NULL) && (a_object1 != NULL))
    {
        for (unsigned int i = 0; i < a_object0->m_objects.size(); i++)
        {
            for (unsigned int j = 0; j < a_object1->m_objects.size(); j++)
            {
                m_SWIFTscene->Deactivate(a_object0->m_objects[i].m_SWIFTobjectId, a_object1->m_objects[j].m_SWIFTobjectId);
            }
        }
        return true;
    }
    else
    {
        return false;
    } 
}


//===========================================================================
/*!
     Render world in OpenGL.
*/
//===========================================================================
void cMatchaWorld::render(cRenderOptions& a_options)
{
    // render cWorld
    cWorld::render(a_options);

#ifdef C_USE_OPENGL

    /////////////////////////////////////////////////////////////////////////
    // Render parts that are always opaque
    /////////////////////////////////////////////////////////////////////////
    if (SECTION_RENDER_OPAQUE_PARTS_ONLY(a_options))
    {
        if (m_showContactPoints)
        {
            // render collision normals
            glDisable(GL_LIGHTING);
            glLineWidth(1.0);

            for (int k=0; k<MATCHA_NUM_BUFFERS; k++)
            {
                for (int i=0; i<m_SWIFTnumCollisions; i++)
                {
                    int numContacts = m_SWIFTcollisionEvents[k][i].m_numContacts;
                    for (int j=0; j<numContacts; j++)
                    {
                        cVector3d pos;

                        glBegin(GL_LINES);
                        glColor3f(1.0, 0.0, 0.0);
                        glVertex3dv((const double*)&(m_SWIFTcollisionEvents[k][i].m_contacts[j].m_globalPosition0));
                        glVertex3dv((const double*)&(m_SWIFTcollisionEvents[k][i].m_contacts[j].m_globalPosition1));
                        glEnd();

                        cVector3d normal0 = m_SWIFTcollisionEvents[k][i].m_contacts[j].m_globalPosition1 + 0.02 * m_SWIFTcollisionEvents[k][i].m_contacts[j].m_globalNormal;
                        glBegin(GL_LINES);
                        glColor3f(0.0, 1.0, 0.0);
                        glVertex3dv((const double*)&(m_SWIFTcollisionEvents[k][i].m_contacts[j].m_globalPosition1));
                        glVertex3dv((const double*)&(normal0));
                        glEnd();
                    }
                }
            }

            glEnable(GL_LIGHTING);
        }
    }

#endif  // C_USE_OPENGL

}


//===========================================================================
/*!
     Render world in OpenGL.
*/
//===========================================================================
void cMatchaWorld::constrainedAcceleration(const Eigen::Vector3d &a, const Eigen::Vector3d &alpha,
                                             Eigen::Vector3d &ac, Eigen::Vector3d &alphac)
{

    // set up typing for projection algorithm
    typedef long double number;
    typedef CWilhelmsenProjection<number>::vector6 vector6;

    // set up a mass matrix for conversion to Euclidean space
//    const double m = 1.0, r = 0.2;
//    const double I = 0.4 * m * r * r;
    vector6 M;
    for (int i = 0; i < 3; ++i) 
    {
        M[i]    = sqrt(m_mass[i]);
        M[i+3]  = sqrt(m_moment[i]);
    }
//    M[0] = M[1] = M[2] = sqrt(m);
//    M[3] = M[4] = M[5] = sqrt(I);

    // set up target point based on desired acceleration
    vector6 b;
    for (int i = 0; i < 3; ++i) {
        b[i]    = M[i] * a[i];
        b[i+3]  = M[i+3] * alpha[i];
    }

    // a global scale for the projection, used for conditioning
    number scale = CWilhelmsenProjection<number>::norm(b);
    if (scale > 1e-10) {
        b /= scale;
    }
    else {
        ac.Zero();
        alphac.Zero();
        return;
    }

    // set up constraint matrix from contact set
    std::vector<vector6> C;
    for (unsigned int i = 0; i < m_contactNormals.size(); ++i)
    {
        const Eigen::Vector3d &n = m_contactNormals[i];
        Eigen::Vector3d k = m_contactPoints[i].cross(n);

        vector6 c;
        for (int j = 0; j < 3; ++j) {
            c[j] = -n[j] / M[j];
            c[j+3] = -k[j] / M[j+3];
        }
        C.push_back(c);
    }

    // condition the set of constraint
    m_status = true;
    CWilhelmsenProjection<number>::conditionGenerators(C);
    m_status = false;

    // solve the projection
    vector6 p = CWilhelmsenProjection<number>::projectCone(C, b);
    vector6 c = scale * (b - p);
    Eigen::Vector3d bc, betac;
    for (int i = 0; i < 3; ++i) 
    {
        bc[i] = c[i] / M[i];
        betac[i] = c[i+3] / M[i+3];
    }

    // return values
    ac = bc;
    alphac = betac;
}

//------------------------------------------------------------------------------
} // namespace chai3d
//------------------------------------------------------------------------------

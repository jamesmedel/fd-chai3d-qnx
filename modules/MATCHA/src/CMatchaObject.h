//==============================================================================
/*
    Software License Agreement (BSD License)
    Copyright (c) 2003-2016, CHAI3D
    (www.chai3d.org)

    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

    * Neither the name of CHAI3D nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
    COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE. 

    \author    <http://www.chai3d.org>
    \author    Francois Conti
    \version   3.3.0-pre+gc11dda393
*/
//==============================================================================

//------------------------------------------------------------------------------
#ifndef CMatchaObjectH
#define CMatchaObjectH
//------------------------------------------------------------------------------
#include "chai3d.h"
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
namespace chai3d {
//------------------------------------------------------------------------------

//==============================================================================
/*!
    \file       CMatchaObject.h

    \brief
    <b> Matcha Module </b> \n 
    Matcha Object.
*/
//==============================================================================

//------------------------------------------------------------------------------
class cMatchaWorld;
//------------------------------------------------------------------------------

struct cMatchaObjectId
{
    int m_SWIFTobjectId;
    cGenericObject* m_object;
};

//==============================================================================
/*!
    \class      cMatchaObject
    \ingroup    Matcha

    \brief
    This class implements a base class for modelling a Matcha object.

    \details
    cMatchaGenericBody is a base class for modeling a Matcha object.
*/
//==============================================================================
class cMatchaObject
{
    //--------------------------------------------------------------------------
    // CONSTRUCTOR & DESTRUCTOR:
    //--------------------------------------------------------------------------

public:

    //! Constructor of cMatchaObject.
    cMatchaObject(cMatchaWorld* a_world) { initialize(a_world); }

    //! Destructor of cMatchaObject.
    virtual ~cMatchaObject();


    //--------------------------------------------------------------------------
    // PUBLIC METHODS:
    //--------------------------------------------------------------------------

public:

    //! This method updates the position and orientation from the Bullet model to CHAI3D model.
    virtual void updateBodyPosition(void) {}

    //! This method loads a collision file.
    virtual bool loadCollisionFile(std::string a_filename, cGenericObject* a_object, double a_scale = 1.0);


    //--------------------------------------------------------------------------
    // PUBLIC MEMBERS:
    //--------------------------------------------------------------------------

public:

    //! Matcha world.
    cMatchaWorld* m_matchaWorld;

    //! SWIFT object ID
    std::vector<cMatchaObjectId> m_objects;


    //--------------------------------------------------------------------------
    // PROTECTED METHODS:
    //--------------------------------------------------------------------------

protected:

    //! This method initializes the Matcha object.
    virtual void initialize(cMatchaWorld* a_world);
};

//------------------------------------------------------------------------------
} // namespace chai3d
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#endif
//------------------------------------------------------------------------------

//==============================================================================
/*
    Software License Agreement (BSD License)
    Copyright (c) 2003-2014, CHAI3D.
    (www.chai3d.org)

    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

    * Neither the name of CHAI3D nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
    COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE. 

    \author    <http://www.chai3d.org>
    \author    Sonny Chan
    \version   3.3.0-pre+gc11dda393
*/
//==============================================================================

//------------------------------------------------------------------------------
#ifndef CWILHELMSENPROJECTION_H
#define CWILHELMSENPROJECTION_H
//------------------------------------------------------------------------------
#include <vector>
#include <Eigen/Core>
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------

// This class is an implementation of the convex cone projection algorithm
// described by Don R. Wilhelmsen in "A nearest point algorithm for convex
// polyhedral cones and applications to positive linear approximation,"
// Mathematics of Computation 30(133), 1976.

template <class T>          // class is templated for double and long double types
class CWilhelmsenProjection
{
    // a debugging variable that holds the set of active constraints
    static std::vector<int> constraints;

public:
    // This implementation works (only) on 6-dimensional vectors

    typedef Eigen::Matrix<T, 6, 1>                              vector6;
    typedef Eigen::Matrix<T, Eigen::Dynamic, 1>                 vectorn;
    typedef Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>    matrixn;
    template <class V> static T dot(const V &a, const V &b)     { return a.dot(b); }
    template <class V> static T norm(const V &v)                { return v.norm(); }
    template <class V> static T norm2(const V &v)               { return v.squaredNorm(); }
    template <class V> static T smallest(const V &v)            { return v.minCoeff(); }
    static vector6 zero6()                                      { return vector6::Zero(); }

    // epsilon values to (hopefully) robustify zero-checking
    static const T eps, eps2;

    // Computes the Euclidean projection of a point q onto a subspace S, the
    // span of the basis vectors given. Returns lambda, the set of barycentric
    // coordinates for the projected point, in terms of the basis of S.
    static vectorn projectSubspace(const std::vector<vector6> &S,
        const vector6 &q);

    // Computes the Euclidean projection of a point q onto a convex cone K,
    // given by a set of generating vectors. Returns a point p in K that is
    // closest to q in terms of Euclidean distance.
    static vector6 projectCone(const std::vector<vector6> &K,
        const vector6 &q);

    // Conditions a set of generating vectors by normalizing them and removing
    // any near-parallel vectors.
    static void conditionGenerators(std::vector<vector6> &K);

    // prints the active constraint set, for debugging
    static void printConstraints();
};


//------------------------------------------------------------------------------
#endif 
//------------------------------------------------------------------------------

//==============================================================================
/*
    Software License Agreement (BSD License)
    Copyright (c) 2003-2016, CHAI3D
    (www.chai3d.org)

    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

    * Neither the name of CHAI3D nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
    COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE. 

    \author    <http://www.chai3d.org>
    \author    Francois Conti
    \version   3.3.0-pre+gc11dda393
*/
//==============================================================================

//------------------------------------------------------------------------------
#include "CMatchaMesh.h"
//------------------------------------------------------------------------------
#include "CMatchaWorld.h"
//------------------------------------------------------------------------------
#include "chai3d.h"
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
namespace chai3d {
//------------------------------------------------------------------------------

//==============================================================================
/*!
    Update collision model position.
*/
//==============================================================================
void cMatchaMesh::updateCollisionModelPosition()
{
    cVector3d pos = getLocalPos();
    cMatrix3d rot = getLocalRot();

    SWIFT_Real R[9];
    SWIFT_Real T[3];
    if (m_objects.size() > 0)
    {
        R[0] = rot(0,0); R[1] = rot(0,1); R[2] = rot(0,2);
        R[3] = rot(1,0); R[4] = rot(1,1); R[5] = rot(1,2);
        R[6] = rot(2,0); R[7] = rot(2,1); R[8] = rot(2,2);
        T[0] = pos(0);   T[1] = pos(1);   T[2] = pos(2);

        m_matchaWorld->m_SWIFTscene->Set_Object_Transformation(m_objects[0].m_SWIFTobjectId, R, T);
    }
}


//==============================================================================
/*!
    This method assigns a desired position to the object.

    \param  a_position  New desired position.
*/
//==============================================================================
void cMatchaMesh::setLocalPos(const cVector3d& a_position)
{
    m_localPos = a_position;
    updateCollisionModelPosition();
}


//==============================================================================
/*!
    This method assigns a desired rotation to the object.

    \param  a_rotation  New desired orientation.
*/
//==============================================================================
void cMatchaMesh::setLocalRot(const cMatrix3d& a_rotation)
{
    m_localRot = a_rotation;
    updateCollisionModelPosition();
}


//------------------------------------------------------------------------------
} // namespace chai3d
//------------------------------------------------------------------------------
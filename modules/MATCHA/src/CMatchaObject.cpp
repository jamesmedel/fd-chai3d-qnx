//==============================================================================
/*
    Software License Agreement (BSD License)
    Copyright (c) 2003-2016, CHAI3D
    (www.chai3d.org)

    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

    * Neither the name of CHAI3D nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
    COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE. 

    \author    <http://www.chai3d.org>
    \author    Francois Conti
    \version   3.3.0-pre+gc11dda393
*/
//==============================================================================

//------------------------------------------------------------------------------
#include "CMatchaObject.h"
//------------------------------------------------------------------------------
#include "CMatchaWorld.h"
#include "CMatchaMesh.h"
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
namespace chai3d {
//------------------------------------------------------------------------------

//==============================================================================
/*!
    This method initializes the Matcha object.

    \param  a_world  Matcha world to which this new object belongs to.
*/
//==============================================================================
void cMatchaObject::initialize(cMatchaWorld* a_world)
{
    // store reference to bullet world
    m_matchaWorld = a_world;

    // no swift object defined yet
    m_objects.clear();

    // add body to world
    m_matchaWorld->m_bodies.push_back(this);
}


//==============================================================================
/*!
    This method is the destructor.
*/
//==============================================================================
cMatchaObject::~cMatchaObject()
{
}


//===========================================================================
/*!
   This method loads a collision file.

   \param  a_filename  Collision file
*/
//===========================================================================
bool cMatchaObject::loadCollisionFile(std::string a_filename, cGenericObject* a_object, double a_scale)
{
    cMatchaObjectId object;
    object.m_object = a_object;

    if ((a_filename != "") && (m_matchaWorld->m_SWIFTscene != NULL))
    {
        if (m_matchaWorld->m_SWIFTscene->Add_General_Object(
                        a_filename.c_str(),
                        object.m_SWIFTobjectId,
                        false,
                        DEFAULT_ORIENTATION,
                        DEFAULT_TRANSLATION,
                        a_scale,
                        DEFAULT_SPLIT_TYPE,
                        DEFAULT_BOX_SETTING,
                        DEFAULT_BOX_ENLARGE_REL,
                        DEFAULT_BOX_ENLARGE_ABS,
                        DEFAULT_CUBE_ASPECT_RATIO)
            )
        {
            // store pointer to object
            m_matchaWorld->m_SWIFTid[object.m_SWIFTobjectId] = a_object;

            // deactivate object for now
            m_matchaWorld->m_SWIFTscene->Deactivate(object.m_SWIFTobjectId);

            // add object to list
            m_objects.push_back(object);

            return (true);
        }
        else
        {
            return (false);
        }
    }
    else
    {
        return (false);
    }
}


//------------------------------------------------------------------------------
} // namespace chai3d
//------------------------------------------------------------------------------

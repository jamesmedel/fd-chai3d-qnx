// version info
#define CHAI3D_ODE_DESCRIPTION     "ODE wrapper module for CHAI3D"
#define CHAI3D_ODE_VERSION_MAJOR   1
#define CHAI3D_ODE_VERSION_MINOR   0
#define CHAI3D_ODE_VERSION_PATCH   0
#define CHAI3D_ODE_PRERELEASE      "-pre"
#define CHAI3D_ODE_METADATA        "+gc11dda393"
#define CHAI3D_ODE_VERSION         "1.0.0-pre+gc11dda393"
#define CHAI3D_ODE_VERSION_FULL    "1.0.0-pre+gc11dda393"
#define CHAI3D_ODE_REVISION        0xc11dda393
